package com.example.tephy.my_calculator;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Entity;
import android.content.Intent;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


public class MainActivity extends Activity implements OnItemSelectedListener {
    TextView mTextView;

    EditText nroone;
    EditText nrotwo;
    Button toAdmin;
    String valueOperation;
    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nroone = (EditText)findViewById(R.id.editText);
        nrotwo = (EditText)findViewById(R.id.editText2);
        toAdmin = (Button)findViewById(R.id.buttonToAdmin);

        toAdmin.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent webSite = new Intent(Intent.ACTION_VIEW);
                webSite.setData(Uri.parse("http://45.55.8.197:12123/admin/"));
                startActivity(webSite);
            }
        });

        mTextView = (TextView) findViewById(R.id.textView1);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        value = (String)spinner.getSelectedItem();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.operation_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        final Button loadButton = (Button) findViewById(R.id.button);
        loadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new HttpGetTask().execute();

            }
        });
    }

    private class HttpGetTask extends AsyncTask<Void, Void, String> {

        //My ip http://45.55.8.197:12123/?a=123&b=2&operator=m

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        String value = (String)spinner.getSelectedItem();


        String datoone = nroone.getText().toString();
        String datotwo = nrotwo.getText().toString();
        private static final String TAG = "HttpGetTask";

//        private String URL = "https://api.easyfolio.de/easy30/performanceData/?format=json";
        @Override
        protected String doInBackground(Void... params) {
            String URL = " http://45.55.8.197:12123/?a="+datoone+"&b="+datotwo+"&operator=";
            if(value.equals("Suma")){
                URL += "a";
            } else if(value.equals("Resta")){
                URL += "s";
            } else if(value.equals("Multiplicacion")){
                URL += "m";
            } else {
                URL += "d";
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(URL));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    responseString = out.toString();
                    out.close();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            Log.e("response", responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            mTextView.setText(result);
        }

        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuffer data = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    data.append(line);
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException");
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException");
                    }
                }
            }
            return data.toString();
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        String item = parent.getItemAtPosition(pos).toString();
        if (item == "Suma" )
        {
            valueOperation = "a";
            item = valueOperation;
        }
        else if(item == "Resta"){
            valueOperation = "s";
            item = valueOperation;
        }
        else if(item== "Multiplicación"){
            valueOperation = "m";
            item = valueOperation;
        }
        else if (item == "División"){
            valueOperation = "d";
            item = valueOperation;
        }
        //Toast.makeText(parent.getContext(),"Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

}
